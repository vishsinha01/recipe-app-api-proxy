# recipe-app-api-proxy

Recipe app API project

NGINX proxy app for our recipe app api

## Usages

### Env variables

- `LISTEN_PORT` - port to listen , default is 8080
- `APP_HOST` - Host name of the app , default is app
- `APP_PORT` - Port to access the app, default is 9000
