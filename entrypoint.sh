#!/bin/sh

set -e

envsubst < /etc/nginx/default.comf.tpl > /etc/nginx/conf.d/default.conf

nginx -g 'daemon off;'